# jest-the-tests #

* various [jest](https://jestjs.io/) tests

# install / run
```
npm install
npm run test
```

# resources #

* [jest](https://jestjs.io/)
* [Taking Advantage of Jest Matchers (Part 1)](https://benmccormick.org/2017/08/15/jest-matchers-1/)
* [Taking Advantage of Jest Matchers (Part 2)](https://benmccormick.org/2017/09/04/jest-matchers-2/)