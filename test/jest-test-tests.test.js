
describe('first', () => {
    test('test true', () => {
        expect(true).toBeTruthy();
    });
})

describe('Taking Advantage of Jest Matchers (Part 1)', () => {
    test('Math still works', () => {
        expect(1 + 1).toBe(2);
    })
    const getRecord = () => ({ id: 1, 'fullName': 'Ben McCormick' });
    test('records have a name and an ID', () => {
        const record = getRecord(1);
        expect(record).toHaveProperty('id');
        expect(record).toHaveProperty('fullName');
        expect(record).not.toHaveProperty('bogusname');
    })
})

describe('Equality Matchers', () => {
    test('toBe and toEqual', () => {
        expect(1 + 1).toBe(2) // true
        expect('on' + 'e').toBe('one') // true
        expect(1 === 1).toBe(true) // true
        const oneObj = { one: 1 };
        expect(oneObj).toBe(oneObj) // true
        expect(oneObj).not.toBe({ one: 1 }) // false!

    })
    test('toEqual', () => {
        let oneObj = { one: 1 };
        let nestedObj = { nested: { one: 1 } };
        expect(oneObj).toEqual({ one: 1 }) // true
        expect(nestedObj).toEqual({ nested: { one: 1 } }) // true
    })
    test('toBeDefined, toBeNull and toBeUndefined', () => {

        expect(null).toBeNull()
        expect(undefined).toBeUndefined()
        expect().toBeUndefined()
        expect(undefined).not.toBeDefined()
    })
    test('toBeFalsy and toBeTruthy', () => {
        let x = true;
        // same as expect(!!x).toBe(true)
        expect(x).toBeTruthy()
        // same as expect(!!x).toBe(false)
        expect(x).not.toBeFalsy()
    })
})
describe('Template matchers', () => {
    test('toMatch', () => {
        let id1 = '155-60-7723';
        let id2 = '15-60-7723';
        let socialSecurityNumberFormat = /\d{3}-\d{2}-\d{4}/;
        expect(id1).toMatch(socialSecurityNumberFormat); // true
        expect(id2).not.toMatch(socialSecurityNumberFormat); // false
    })
    test('toMatchObject and toHaveProperty', () => {
        let oneObj = { one: 1, uno: 1 };
        expect(oneObj).not.toEqual({ one: 1 })
        expect(oneObj).toMatchObject({ one: 1 })
        expect(oneObj).toMatchObject({ uno: 1 })
        expect(oneObj).toMatchObject({ uno: 1, one: 1 })
        expect(oneObj).not.toMatchObject({ un: 1 })
        expect(oneObj).toHaveProperty('one')
        expect(oneObj).toHaveProperty('uno')
        expect(oneObj).not.toHaveProperty('fred')
        expect(oneObj).toHaveProperty('one', 1)
        expect(oneObj).not.toHaveProperty('one', 2)
    })
    test('toHaveLength', () => {
        expect('Hello').toHaveLength(5)
        expect(['Hello', 'From', 'The', 'Other', 'Side']).toHaveLength(5)
        expect({ length: 5 }).toHaveLength(5)
    })
    test('toBeInstanceOf', () => {
        class A { }
        class B extends A { }
        class C { }

        let a = new A();
        let b = new B();
        let c = new C();
        expect(a).toBeInstanceOf(A);
        expect(b).toBeInstanceOf(A);
        expect(c).not.toBeInstanceOf(A);
    })
    test('toThrow', () => {
        const throwingFunc1 = () => {
            throw 'error';
        };
        class SpecialError { }
        const throwingFunc2 = () => {
            throw new SpecialError()
        };

        expect(throwingFunc1).toThrow('err');
        expect(throwingFunc1).toThrow(/err/);
        expect(throwingFunc2).toThrow(SpecialError);
    })
})