// https://momentjs.com/docs/#/parsing/

const moment = require('moment')
moment().format()


describe('ISO 8601 strings', () => {
    const expDateString='Fri Feb 08 2013 00:00:00 GMT-0700'
    test('yyyy-mm-dd calendar date part', () => {
        expect(moment('2013-02-08').toString()).toBe(expDateString)
    })
    test('week date part', () => {
        expect(moment('2013-W06-5').toString()).toBe(expDateString)
    })
    test('ordinal part', () => {
        expect(moment('2013-039').toString()).toBe(expDateString)
    })
    test('Basic (short) full date', () => {
        expect(moment('20130208').toString()).toBe(expDateString)
    })
    test('Basic (short) week, weekday', () => {
        expect(moment('2013W065').toString()).toBe(expDateString)
    })
    test('Basic (short) week only', () => {
        expect(moment('2013W06').toString()).toBe('Mon Feb 04 2013 00:00:00 GMT-0700')
    })
    test('Basic (short) ordinal date', () => {
        expect(moment('2013050').toString()).toBe('Tue Feb 19 2013 00:00:00 GMT-0700')
    })
})
describe('String+Format',()=>{
    test('equal stuff',()=>{
        expect(moment("12-25-1995", "MM-DD-YYYY")).toEqual(moment("12-25-1995", "MM-DD-YYYY"))
    })
    test('output',()=>{
        expect(moment(undefined)).toBeTruthy()
        let d1=moment({ years: 2010, months: 2, days: 5, hours: 15, minutes: 10, seconds: 3, milliseconds: 123 })
        let d2=moment({ years: 2010, months: 2, days: 5, hours: 15, minutes: 10, seconds: 3, milliseconds: 123 })
        expect(d1).toEqual(d2)
        d1=moment("20140101", "YYYYMMDD", true)
        d2=moment("2014-01-01", "YYYY-MM-DD", true)
        expect(""+d1).toEqual(""+d2)
        expect(moment('It is 2012-05-25', 'YYYY-MM-DD').isValid()).toBeTruthy()
        expect(moment('2016 is a date', 'YYYY-MM-DD').year()).toBe(2016)
        expect(moment("2010-10-20 4:30 pm","YYYY-MM-DD HH:mm a").month()).toBe(9)
        if(moment('2016 is a date', 'YYYY-MM-DD').isValid()) {
        }
        expect(moment().month("February").quarter()).toBe(1)
        expect(moment().month("April").quarter()).toBe(2)
        expect(moment().month("August").quarter()).toBe(3)
        expect(moment().month("November").quarter()).toBe(4)
    })
})


describe('placeholder test', () => {
    test('just a place', () => {
        expect(4).toBe(4)
    });
});